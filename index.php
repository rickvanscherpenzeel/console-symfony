<?php
/**
 * Created by PhpStorm.
 * User: ricks
 * Date: 16-3-15
 * Time: 13:29
 */

#!/usr/bin/env php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Ibg\Console\Command\GreetCommand;

$application = new Application();
$application->add(new GreetCommand);
$application->run();